

--[[
__  __                  _   _ 
|  \/  | __ _ _ __ _   _| |_(_)
| |\/| |/ _` | '__| | | | __| |
| |  | | (_| | |  | |_| | |_| |
|_|  |_|\__,_|_|   \__,_|\__|_|
  
--]]


local set = vim.opt
local global = vim.g
local keymap = vim.keymap
local cmd = vim.cmd

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--[[
   ____  ____  ______________  _   _______
  / __ \/ __ \/_  __/  _/ __ \/ | / / ___/
 / / / / /_/ / / /  / // / / /  |/ /\__ \ 
/ /_/ / ____/ / / _/ // /_/ / /|  /___/ / 
\____/_/     /_/ /___/\____/_/ |_//____/  
    
--]]



set.number = true
set.tabstop = 4
set.shiftwidth = 4
set.softtabstop = 0
set.expandtab = true
set.swapfile = false
set.relativenumber = true
set.termguicolors = true
set.incsearch = true
set.ruler = true
set.backup = false
set.ignorecase = true
set.hlsearch  = true
set.fileencoding = "utf-8"
set.mouse = "a"
set.splitbelow = true
set.splitright = true
set.undodir = '/home/vagrant/.vim/undodir' 
set.undofile = true
set.smartindent = true
set.smartcase = true
set.writebackup = false
set.updatetime = 300
set.signcolumn = "yes"
set.bg = "dark"
set.autochdir = true
 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


--[[
   __________  _   ____________________  ______  ___  ______________  _   __
  / ____/ __ \/ | / / ____/  _/ ____/ / / / __ \/   |/_  __/  _/ __ \/ | / /
 / /   / / / /  |/ / /_   / // / __/ / / / /_/ / /| | / /  / // / / /  |/ / 
/ /___/ /_/ / /|  / __/ _/ // /_/ / /_/ / _, _/ ___ |/ / _/ // /_/ / /|  /  
\____/\____/_/ |_/_/   /___/\____/\____/_/ |_/_/  |_/_/ /___/\____/_/ |_/   
                                                                            
 
--]]

--- Nvim-Tree
--
-- disable netrw at the very start of your init.lua (strongly advised)
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

require("nvim-tree").setup({
  sort_by = "case_sensitive",
  view = {
    width = 30,
    mappings = {
      list = {
        { key = "u", action = "dir_up" },
      },
    },
  },
  renderer = {
    group_empty = true,
  },
  filters = {
    dotfiles = false,
  },
})

-- Vim-autopairs
require('nvim-autopairs').setup({
  disable_filetype = { "TelescopePrompt" , "vim" },
})

-- Coc-nvim
vim.cmd [[
let g:coc_global_extensions = ['coc-emmet', 'coc-go', 'coc-html', 'coc-eslint', 'coc-git', 'coc-json', 'coc-pyright', 'coc-tsserver', 'coc-prettier', 'coc-yaml','coc-ltex','coc-rust-analyzer','coc-docker', 'coc-css']"
]]




-- Lualine

local lualine = require('lualine')

-- Color table for highlights
-- stylua: ignore
local colors = {
  bg       = '#202328',
  fg       = '#bbc2cf',
  yellow   = '#ECBE7B',
  cyan     = '#008080',
  darkblue = '#081633',
  green    = '#98be65',
  orange   = '#FF8800',
  violet   = '#a9a1e1',
  magenta  = '#c678dd',
  blue     = '#51afef',
  red      = '#ec5f67',
}

local conditions = {
  buffer_not_empty = function()
    return vim.fn.empty(vim.fn.expand('%:t')) ~= 1
  end,
  hide_in_width = function()
    return vim.fn.winwidth(0) > 80
  end,
  check_git_workspace = function()
    local filepath = vim.fn.expand('%:p:h')
    local gitdir = vim.fn.finddir('.git', filepath .. ';')
    return gitdir and #gitdir > 0 and #gitdir < #filepath
  end,
}

-- Config
local config = {
  options = {
    -- Disable sections and component separators
    component_separators = '',
    section_separators = '',
    theme = {
      -- We are going to use lualine_c an lualine_x as left and
      -- right section. Both are highlighted by c theme .  So we
      -- are just setting default looks o statusline
      normal = { c = { fg = colors.fg, bg = colors.bg } },
      inactive = { c = { fg = colors.fg, bg = colors.bg } },
    },
  },
  sections = {
    -- these are to remove the defaults
    lualine_a = {},
    lualine_b = {},
    lualine_y = {},
    lualine_z = {},
    -- These will be filled later
    lualine_c = {},
    lualine_x = {},
  },
  inactive_sections = {
    -- these are to remove the defaults
    lualine_a = {},
    lualine_b = {},
    lualine_y = {},
    lualine_z = {},
    lualine_c = {},
    lualine_x = {},
  },
}

-- Inserts a component in lualine_c at left section
local function ins_left(component)
  table.insert(config.sections.lualine_c, component)
end

-- Inserts a component in lualine_x ot right section
local function ins_right(component)
  table.insert(config.sections.lualine_x, component)
end

ins_left {
  function()
    return '▊'
  end,
  color = { fg = colors.blue }, -- Sets highlighting of component
  padding = { left = 0, right = 1 }, -- We don't need space before this
}

ins_left {
  -- mode component
  function()
    return ''
  end,
  color = function()
    -- auto change color according to neovims mode
    local mode_color = {
      n = colors.red,
      i = colors.green,
      v = colors.blue,
      [''] = colors.blue,
      V = colors.blue,
      c = colors.magenta,
      no = colors.red,
      s = colors.orange,
      S = colors.orange,
      [''] = colors.orange,
      ic = colors.yellow,
      R = colors.violet,
      Rv = colors.violet,
      cv = colors.red,
      ce = colors.red,
      r = colors.cyan,
      rm = colors.cyan,
      ['r?'] = colors.cyan,
      ['!'] = colors.red,
      t = colors.red,
    }
    return { fg = mode_color[vim.fn.mode()] }
  end,
  padding = { right = 1 },
}

ins_left {
  -- filesize component
  'filesize',
  cond = conditions.buffer_not_empty,
}

ins_left {
  'filename',
  cond = conditions.buffer_not_empty,
  color = { fg = colors.magenta, gui = 'bold' },
}

ins_left { 'location' }

ins_left { 'progress', color = { fg = colors.fg, gui = 'bold' } }

ins_left {
  'diagnostics',
  sources = { 'coc' },
  symbols = { error = ' ', warn = ' ', info = ' ' },
  diagnostics_color = {
    color_error = { fg = colors.red },
    color_warn = { fg = colors.yellow },
    color_info = { fg = colors.cyan },
  },
}

-- Insert mid section. You can make any number of sections in neovim :)
-- for lualine it's any number greater then 2
ins_left {
  function()
    return '%='
  end,
}

ins_left {
  -- Lsp server name .
  function()
    local msg = 'No Active Lsp'
    local buf_ft = vim.api.nvim_buf_get_option(0, 'filetype')
    local clients = vim.lsp.get_active_clients('coc')
    if next(clients) == nil then
      return msg
    end
    for _, client in ipairs(clients) do
      local filetypes = client.config.filetypes
      if filetypes and vim.fn.index(filetypes, buf_ft) ~= -1 then
        return client.name
      end
    end
    return msg
  end,
  icon = ' LSP:',
  color = { fg = '#ffffff', gui = 'bold' },
}

-- Add components to right sections
ins_right {
  'o:encoding', -- option component same as &encoding in viml
  fmt = string.upper, -- I'm not sure why it's upper case either ;)
  cond = conditions.hide_in_width,
  color = { fg = colors.green, gui = 'bold' },
}

ins_right {
  'fileformat',
  fmt = string.upper,
  icons_enabled = false, -- I think icons are cool but Eviline doesn't have them. sigh
  color = { fg = colors.green, gui = 'bold' },
}

ins_right {
  'branch',
  icon = '',
  color = { fg = colors.violet, gui = 'bold' },
}

ins_right {
  'diff',
  -- Is it me or the symbol for modified us really weird
  symbols = { added = ' ', modified = '柳 ', removed = ' ' },
  diff_color = {
    added = { fg = colors.green },
    modified = { fg = colors.orange },
    removed = { fg = colors.red },
  },
  cond = conditions.hide_in_width,
}

ins_right {
  function()
    return '▊'
  end,
  color = { fg = colors.blue },
  padding = { left = 1 },
}

lualine.setup(config)

-- Floaterm



-- Bufferline
require("bufferline").setup{
    options = {
         diagnostics = "coc",
         diagnostics_indicator = function(count, level, diagnostics_dict, context)
             local icon = level:match("error") and " " or " "
             return " " .. icon .. count
         end,
         offsets = {
                {
                    filetype = "NvimTree",
                    text = "File Explorer",
                    text_align = "left",
                    separator = true,
                    highlight = "Directory",
                }
            },
    }
}  

--COC
cmd "let g:coc_global_extensions = [ 'coc-emmet', 'coc-go', 'coc-html', 'coc-eslint', 'coc-git', 'coc-json', 'coc-pyright', 'coc-tsserver', 'coc-prettier', 'coc-yaml','coc-ltex','coc-angular', 'coc-markdownlint']"


--  MINIMAP
require('mini.map').setup()

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--[[
  ________  __________  ______________
 /_  __/ / / / ____/  |/  / ____/ ___/
  / / / /_/ / __/ / /|_/ / __/  \__ \ 
 / / / __  / /___/ /  / / /___ ___/ / 
/_/ /_/ /_/_____/_/  /_/_____//____/  
                                      
--]]

cmd("colorscheme murphy")

-->> I use default colorscheme as these are enough for good colorscheme.

--[[
blue
darkblue
default
delek 
desert
elflord
evening
gruvbox
habamax
industry
koehler
lunaperche
minicyan
minischeme
morning
murphy
pablo
peachpuff
quiet
ron
shine
slate
torte
zellner
--]]

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



--[[
    __ __ ________  ____  ______    ____ 
   / //_// ____/\ \/ /  |/  /   |  / __ \
  / ,<  / __/    \  / /|_/ / /| | / /_/ /
 / /| |/ /___    / / /  / / ___ |/ ____/ 
/_/ |_/_____/   /_/_/  /_/_/  |_/_/      
    
--]]
--
--
global.mapleader = " "
--
--
-- Save,exit and source file
keymap.set("n", "<leader>w", ":w<CR>", {noremap = true})
keymap.set("i", "<C-s>", "<esc>:w<CR>", {noremap = true})
keymap.set("n", "<leader>s", ":so % | :nohl<CR>", {noremap = true})
keymap.set("n", "<leader>q", ":q!<CR>", {noremap = true})

-- Open my neovim config whenever i call it
keymap.set("n", "<leader>ev", ":vsplit $MYVIMRC<CR>", {noremap = true})
--keymap.set("n"

-- Panes/Windows - split. resize and move  
keymap.set("n", "<leader>v", ":vsplit<CR>", {noremap = true})
keymap.set("n", "<leader>h", ":split<CR>", {noremap = true})
keymap.set("n", "<leader><Right>", ":vertical resize +10<CR>", {noremap = true})
keymap.set("n", "<leader><Left>",  ":vertical resize -10<CR>", {noremap = true})
keymap.set("n", "<leader><Up>",    ":resize +10<CR>", {noremap = true})
keymap.set("n", "<leader><Down>", ":resize -10<CR>", {noremap = true})
keymap.set("n", "<tab>", "<C-w><C-w>", {noremap = true})

-- Move between diffrent windows
keymap.set('n', '<A-h>', '<C-w>h',{noremap = true})
keymap.set('n', '<A-j>', '<C-w>j',{noremap = true})
keymap.set('n', '<A-k>', '<C-w>k',{noremap = true})
keymap.set('n', '<A-l>', '<C-w>l',{noremap = true})


-- Open nvim-tree
keymap.set('n', '<leader>b', ':NvimTreeToggle<CR>', {noremap = true})

-- terminal and gitui
keymap.set('t', '<esc>', '<C-\\><C-n>', {noremap = true})
keymap.set('n', '<leader>f', ':Git fetch upstream<CR>', {noremap = true})
keymap.set('n', '<leader>m', ':Git merge upstream/main<CR>', {noremap = true})
keymap.set('n', '<leader>z', ':Git branch<CR>', {noremap = true})
keymap.set('n', '<leader>k', ':BufferLineCycleNext<CR>', {noremap = true})
keymap.set('n', '<leader>j', ':BufferLineCyclePrev<CR>', {noremap = true})
keymap.set('n', '<C-n>', ':FloatermToggle<CR>', {noremap = true})
keymap.set('n', '<leader>g', ':FloatermNew --height=0.9 --width=0.7 --position=bottomright --wintype=float gitui<CR>', {noremap = true})
keymap.set('n', '<leader>n', ':FloatermNew --height=0.9 --width=0.7 --position=bottomright --wintype=float<CR>', {noremap = true})
keymap.set('n', '<leader>a', ':Git add -A<CR>', {noremap = true})
keymap.set('n', '<C-a>', ':Git status<CR>', {noremap = true})



--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



--[[
    ____  __    __  _____________   __
   / __ \/ /   / / / / ____/  _/ | / /
  / /_/ / /   / / / / / __ / //  |/ / 
 / ____/ /___/ /_/ / /_/ // // /|  /  
/_/   /_____/\____/\____/___/_/ |_/   
    
]]


return require('packer').startup(function(use)
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'


  -- Tree-sitter
  use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }


  -- Gruvbox
 use { "ellisonleao/gruvbox.nvim" } 

  -- Vim-fugitive
  use { 'tpope/vim-fugitive'}

  -- Coc
  use { 'neoclide/coc.nvim', branch = 'release' } 

  -- autopair
  use {
	"windwp/nvim-autopairs",
    config = function() require("nvim-autopairs").setup {} end
}

  -- filemanagaer
  use {
  'nvim-tree/nvim-tree.lua',
  requires = {
    'nvim-tree/nvim-web-devicons', -- optional, for file icons
  },
  tag = 'nightly' -- optional, updated every week. (see issue #1193)
}

  -- Floaterm
  use {'voldikss/vim-floaterm'}

  -- Bufferline
  use {'akinsho/bufferline.nvim', tag = "v3.*", requires = 'nvim-tree/nvim-web-devicons'}
  
  -- lualine
  use {
  'nvim-lualine/lualine.nvim',
  requires = { 'kyazdani42/nvim-web-devicons', opt = true }
}
  -- Plenary
  use "nvim-lua/plenary.nvim"

  -- Minimap
  use 'echasnovski/mini.nvim'

  -- Indent line
  -- use "lukas-reineke/indent-blankline.nvim"
 
end)
                                      
      
